import requests
from django.template import engines

import copy

from django_cdstack_deploy.django_cdstack_deploy.func import (
    generate_config_static,
    zip_add_file,
)


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_dc_tun_srv/django_cdstack_tpl_dc_tun_srv"

    r = requests.get(
        template_opts["dc_api_url"]
        + "/api/v1/tunnel/"
        + template_opts["node_hostname_fqdn"],
        headers={
            "Authorization": "ApiKey "
            + template_opts["dc_api_username"]
            + ":"
            + template_opts["dc_api_key"],
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
    )

    dc_tun_data = r.json()

    for server_key, tunnel in dc_tun_data.items():
        tunnel_options = {**template_opts, **tunnel["options"]}
        tunnel_options["network_tinc_id"] = (
            template_opts["node_hostname_fqdn"].replace(".", "_").replace("-", "_")
        )

        config_template_file = open(
            module_prefix
            + "/templates/config-fs/dynamic/etc/tinc/mode-switch/tinc.conf",
            "r",
        ).read()
        config_template = django_engine.from_string(config_template_file)

        zip_add_file(
            zipfile_handler,
            "etc/tinc/dctunnel-" + server_key + "/tinc.conf",
            config_template.render(tunnel_options),
        )

        config_template_file = open(
            module_prefix
            + "/templates/config-fs/dynamic/etc/tinc/mode-switch/rsa_key.priv",
            "r",
        ).read()
        config_template = django_engine.from_string(config_template_file)

        zip_add_file(
            zipfile_handler,
            "etc/tinc/dctunnel-" + server_key + "/rsa_key.priv",
            config_template.render(tunnel_options),
        )

        config_template_file = open(
            module_prefix + "/templates/config-fs/dynamic/etc/tinc/mode-switch/tinc-up",
            "r",
        ).read()
        config_template = django_engine.from_string(config_template_file)

        zip_add_file(
            zipfile_handler,
            "etc/tinc/dctunnel-" + server_key + "/tinc-up",
            config_template.render(tunnel_options),
        )

        config_template_file = open(
            module_prefix
            + "/templates/config-fs/dynamic/etc/tinc/mode-switch/tinc-down",
            "r",
        ).read()
        config_template = django_engine.from_string(config_template_file)

        zip_add_file(
            zipfile_handler,
            "etc/tinc/dctunnel-" + server_key + "/tinc-down",
            config_template.render(tunnel_options),
        )

        config_template_file = open(
            module_prefix
            + "/templates/config-fs/dynamic/etc/tinc/mode-switch/hosts/peer-local",
            "r",
        ).read()
        config_template = django_engine.from_string(config_template_file)

        zip_add_file(
            zipfile_handler,
            "etc/tinc/dctunnel-"
            + server_key
            + "/hosts/"
            + tunnel_options["network_tinc_id"],
            config_template.render(tunnel_options),
        )

        for client_key, client in tunnel["clients"].items():
            config_template_file = open(
                module_prefix
                + "/templates/config-fs/dynamic/etc/tinc/mode-switch/hosts/peer",
                "r",
            ).read()
            config_template = django_engine.from_string(config_template_file)

            zip_add_file(
                zipfile_handler,
                "etc/tinc/dctunnel-" + server_key + "/hosts/peer" + client_key,
                config_template.render(client["options"]),
            )

    generate_config_static(zipfile_handler, template_opts, module_prefix)

    return True
